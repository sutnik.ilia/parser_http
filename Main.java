package html.parser;

import org.apache.hc.client5.http.fluent.Request;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String contentNews = Request.post("https://www.opennet.ru/opennews/mini.shtml")
                .execute()
                .returnContent()
                .asString();
        String contentBook = Request.post("https://mirknig.su/")
                .setHeader("User-Agent", "MySuperUserAgent")
                .execute()
                .returnContent()
                .asString();
        String[] wordsNews = contentNews.split(" ");
        String[] wordsBook = contentBook.split(" ");
        int count = 0;
        for (String word : wordsNews) {
            if (word.contains("31.08.2021")) {
                count++;
            }
        }
        int countBook = 0;
        for (int i = 0; i < wordsBook.length; i++) {
            if (wordsBook[i].contains("Сегодня")) {
                countBook++;
            }
        }
        System.out.println("Count of News: " + count + ", count of book html: " + countBook);
    }
}
